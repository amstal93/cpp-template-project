Common code
===========

C++ module to gather all common code between all project modules.

****

.. include:: class_view_hierarchy.rst

.. include:: unabridged_api.rst
