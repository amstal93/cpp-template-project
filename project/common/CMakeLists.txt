# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(common
  LANGUAGES
    CXX
  DESCRIPTION
    "A collection of common code."
)

#------------------------------------------------------------------------------#
# Project files                                                                #
#------------------------------------------------------------------------------#

set(API_HEADERS
  floating_point.hpp
  point.hpp
)

list(TRANSFORM API_HEADERS PREPEND "include/common/")
set(API_HEADERS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

set(HEADERS
  ${API_HEADERS}
)

set(SOURCE
  # Add source files...
)

#------------------------------------------------------------------------------#
# Project                                                                      #
#------------------------------------------------------------------------------#

add_library("${PROJECT_NAME}" INTERFACE)
add_library(Ctp::Common ALIAS ${PROJECT_NAME})

source_group("api" FILES ${API_HEADERS})

#target_init("${PROJECT_NAME}")

add_api_docs("${PROJECT_NAME}" "${API_HEADERS_DIR}")
add_docs("${PROJECT_NAME}" "${CMAKE_CURRENT_SOURCE_DIR}/docs")

#------------------------------------------------------------------------------#
# Project directories                                                          #
#------------------------------------------------------------------------------#

target_include_directories("${PROJECT_NAME}"
  INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)
