# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(example
  LANGUAGES
    CXX
  DESCRIPTION
    "Example CLI app for VCS and DocOpt libraries."
)

#------------------------------------------------------------------------------#
# Project files                                                                #
#------------------------------------------------------------------------------#

set(HEADERS
  # Nothing...
)

set(SOURCE
  src/main.cpp
)

#------------------------------------------------------------------------------#
# Project                                                                      #
#------------------------------------------------------------------------------#

add_executable("${PROJECT_NAME}" ${HEADERS} ${SOURCE})
target_init("${PROJECT_NAME}")

#------------------------------------------------------------------------------#
# Project directories                                                          #
#------------------------------------------------------------------------------#

target_include_directories("${PROJECT_NAME}"
  PRIVATE
    "${PROJECT_SOURCE_DIR}"
)

#------------------------------------------------------------------------------#
# Project libraries                                                            #
#------------------------------------------------------------------------------#

target_link_libraries("${PROJECT_NAME}"
  PRIVATE
    Ctp::Vcs
)

target_link_libraries("${PROJECT_NAME}"
  PRIVATE
    docopt::docopt
)
