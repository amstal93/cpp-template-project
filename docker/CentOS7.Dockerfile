# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Never try to install CentOS 7 SCL packages and build with them something in a
# same Docker file. This is just a one big pain. But if you still want...
#
# First of all I tried a below solution, that is based on:
#   - https://stackoverflow.com/questions/20635472
#   - https://stackoverflow.com/questions/55901985
#
# CentOS7.Dockerfile
#   ...
#   RUN echo "source scl_source enable devtoolset-8" >> /etc/bashrc && \
#       echo "source scl_source enable llvm-toolset-7.0" >> /etc/bashrc
#
#   Make SCL avaible globaly in user environment.
#   SHELL ["/bin/bash", "-c", "source /etc/bashrc"]
#   ...
#   # Make SCL avaible to anyone how use this docker image.
#   ENTRYPOINT ["/bin/bash", "-c", "source /etc/bashrc"]
#
# I don't know why but the above Docker file doesn't work at all. It literaly
# breaks GitLab CI. CI doesn't run 'script:' section with a such docker and
# already report successful result.
#
# Working solution is based on https://github.com/sclorg/devtoolset-container/
# docker container for 7-toolchain.

FROM centos:7

WORKDIR /tmp

# Setup system and install packages

COPY scripts/os/setup_centos7.sh .
COPY requirements.txt .
RUN bash setup_centos7.sh

RUN echo "unset BASH_ENV PROMPT_COMMAND ENV" >> /etc/scl_enable && \
    echo "source scl_source enable devtoolset-10" >> /etc/scl_enable && \
    echo "source scl_source enable llvm-toolset-7.0" >> /etc/scl_enable

# Enable the SCL for all bash scripts.
ENV BASH_ENV=/etc/scl_enable \
    ENV=/etc/scl_enable \
    PROMPT_COMMAND=". /etc/scl_enable"

# Fetch Conan dependencies

COPY cmake/Conan.cmake CMakeLists.txt
COPY scripts/fetch_conan_dependencies.sh .
RUN bash fetch_conan_dependencies.sh

# Remove all temporary files

WORKDIR /work
RUN rm -rf /tmp/
